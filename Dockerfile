FROM python:3-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./app.py", "--debug", "--host", "0.0.0.0", "--pluto-url", "http://host.docker.internal:8080"]