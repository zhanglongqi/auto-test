# autotest

## getting started

```shell
git clone git@gitlab.com:zhanglongqi/auto-test.git

cd auto-test
sudo apt install python3 python3-pip
sudo -H pip3 install pipenv

pipenv install
pipenv run python app.py --parallel /dev/ttyUSB0
```

## Packaging

```shell
pipenv install --dev
pipenv run pyinstaller --console --noconfirm --paths . --strip --name autotest app.py 
```
