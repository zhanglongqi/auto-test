colorama==0.4.6 ; python_version >= "3.9" and python_version < "3.12" and sys_platform == "win32" \
    --hash=sha256:08695f5cb7ed6e0531a20572697297273c47b8cae5a63ffc6d6ed5c201be6e44 \
    --hash=sha256:4f1d9991f5acc0ca119f9d443620b77f9d6b33703e51011c16baf57afb285fc6
colorlog==6.7.0 ; python_version >= "3.9" and python_version < "3.12" \
    --hash=sha256:0d33ca236784a1ba3ff9c532d4964126d8a2c44f1f0cb1d2b0728196f512f662 \
    --hash=sha256:bd94bd21c1e13fac7bd3153f4bc3a7dc0eb0974b8bc2fdf1a989e474f6e582e5
minimalmodbus==2.0.1 ; python_version >= "3.9" and python_version < "3.12" \
    --hash=sha256:6b8ad7e52d98fff9912d6a90fdc021138750e281b0c2a8a5563ec8902d849538 \
    --hash=sha256:cf873a2530be3f4b86467c3e4d47b5f69fd345d47451baca4adbf59e2ac36d00
pyserial==3.5 ; python_version >= "3.9" and python_version < "3.12" \
    --hash=sha256:3c77e014170dfffbd816e6ffc205e9842efb10be9f58ec16d3e8675b4925cddb \
    --hash=sha256:c4451db6ba391ca6ca299fb3ec7bae67a5c55dde170964c7a14ceefec02f2cf0
