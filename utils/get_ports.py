#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 6/Jul/17 09:24
Description:


"""
import glob
import sys

import serial
from threading import Lock

lock = Lock()


def serial_ports():
	""" Lists serial port names

			:raises EnvironmentError:
					On unsupported or unknown platforms
			:returns:
					A list of the serial ports available on the system
	"""
	lock.acquire()
	if sys.platform.startswith('win'):
		ports = ['COM%s' % (i + 1) for i in range(256)]
	elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
		# this excludes your current terminal "/dev/ttyUSB"
		ports = glob.glob('/dev/ttyUSB[A-Za-z]*')
	elif sys.platform.startswith('darwin'):
		ports = glob.glob('/dev/cu.*')
	else:
		raise EnvironmentError('Unsupported platform')

	result = []
	for port in ports:
		try:
			s = serial.Serial(port)
			s.close()
			result.append(port)
		except (OSError, serial.SerialException):
			pass
	lock.release()
	return result


if __name__ == '__main__':
	print((serial_ports()))
