#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 20/Mar/17 15:07
Description:


"""
import os
import subprocess
from os.path import join
from tempfile import gettempdir
from time import sleep

from apscheduler.schedulers.background import BackgroundScheduler

with open(join(gettempdir(), 'HGOS.pid'), 'r') as pid_file:
	print(gettempdir())
	HGOS_pid = int(pid_file.readline())
	HGOS_dir = pid_file.readline()

print(HGOS_pid)
print(HGOS_dir)


def canbus_is_down():
	canbus_info = str(subprocess.check_output(['ip', '-s', 'link', 'ls', 'can0']))
	print(canbus_info)
	if 'DOWN' in canbus_info:
		return True
	else:
		return False


def reset_canbus():
	print('Resetting CAN bus ...')
	subprocess.check_output(['ip', 'link', 'set', 'can0', 'down'])
	sleep(2)
	subprocess.check_output(['ip', 'link', 'set', 'can0', 'up'])


def restart_HGOS():
	print('restart HGOS')


def check_canbus():
	if canbus_is_down():
		# kill(HGOS_pid, SIGTERM)
		reset_canbus()


if __name__ == '__main__':

	scheduler = BackgroundScheduler()
	scheduler.add_job(check_canbus, 'interval', seconds=2)
	scheduler.start()
	print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

	try:
		# This is here to simulate application activity (which keeps the main thread alive).
		while True:
			sleep(2)
	except (KeyboardInterrupt, SystemExit):
		# Not strictly necessary if daemonic mode is enabled but should be done if possible
		scheduler.shutdown()
