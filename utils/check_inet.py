from sys import exit
import socket
ERROR_CODE_CHECK_INET_FAIL = 2

def check_internet(host="8.8.8.8", port=53, timeout=3):
	"""
	Host: 8.8.8.8 (google-public-dns-a.google.com)
	OpenPort: 53/tcp
	Service: domain (DNS/TCP)
	"""
	try:
		socket.setdefaulttimeout(timeout)
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, port))
	except OSError as e:
		print(e.strerror)
		return False
	else:
		s.close()
		return True

if __name__ == "__main__":
	if check_internet():
		print('Internet is OK')
		exit(0)
	else:
		print('NO Internet')
		exit(ERROR_CODE_CHECK_INET_FAIL)