#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
from utils import setup_logger


class StateMachine():
	priority = 0

	def __init__(self, logger=None, timeout: int = None, debug: bool = False):
		self.logger = logger if logger is not None else setup_logger(name='StateMachine', verbose=2 if debug else 1)

		self.name = None
		self.states = {'starting': State(name='starting'), 'end': State(name='end'), 'error': State(name='error')}
		self.states['starting'].run = self.starting
		self.state = self.states['starting']

		self.states['error'].run = self.error
		self.states['error'].transitions = {'STAY': self.states['error']}
		self.start_time = time.time()
		self.spent_t = 0
		self.TIMEOUT = timeout

		self.paused = False
		self.last_state = self.states['starting']

	def run(self):
		assert self.state
		if self.paused or self.state == self.states['end'] or self.state == self.states['error']:
			return

		self.last_state = self.state
		self.state = self.state.next(self.state.run(**self.state.kwargs))
		if self.state is None:
			self.logger.error(f'{self.last_state} no valid next state')
			self.state = self.states['error']
			return
		# print(self.state)
		if self.last_state == self.state:
			self.logger.debug(f'{self.state} spent {time.time() - self.state.timer}s')
		else:
			self.state.timer = time.time()
			self.last_state.timer = 0
			self.logger.debug(f'{self.last_state} -> {self.state}')

	def starting(self):
		assert 'It needs to be override.'

	def pause(self):
		self.paused = True

	def resume(self):
		self.paused = False

	def error(self):
		self.logger.error('error', self.last_state)
		return 'STAY'

	def reset(self):
		self.state = self.states['starting']
		self.start_time = time()
		self.spent_t = 0
		for name, state in self.states.items():
			state.spent_t = 0

	def is_current_state_timeout(self):
		if self.state.TIMEOUT is None or time.time() - self.state.timer <= self.state.TIMEOUT:
			return False
		elif time.time() - self.state.timer > self.state.TIMEOUT:
			self.state.timer = time.time()
			self.logger.debug((self.state.name, 'timeout.'))
			return True

	def is_current_task_timeout(self):
		if self.TIMEOUT is None or self.spent_t <= self.TIMEOUT:
			return False
		elif self.spent_t > self.TIMEOUT:
			self.logger.debug((self.name, 'timeout.'))
			return True


class State():
	def __init__(self, name='', timeout=None):
		self.name = name
		self.TIMEOUT = timeout
		self.transitions = {}

		self.timer = time.time()  # timestamp that the state starts

		self.kwargs = {}

	def __str__(self):
		return self.name

	def reset(self):
		self.spent_t = 0

	def next(self, input):
		if 'STAY' == input:
			return self
		elif input in self.transitions:
			return self.transitions[input]
		else:
			return None

	def run(self, **kwargs):
		pass
