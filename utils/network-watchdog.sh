#!/bin/bash
source utils/commands.sh

time_failed=0
interval=10
threshold=100 #seconds

if (($# == 0)); then
	LOG_ERR "please specify the network"
	exit 1
else
	LOG_NOTICE "predefined wifi is $1"
fi

while true; do
	LOG_INFO "checking /dev/ttyUSB0, internet and VPN"

	r0=0
	if [ ! -c "/dev/ttyUSB0" ]; then
		LOG_ERR "lost USB2RS485"
		r0=2
	fi

	ping -c 1 -W 5 google.com >/dev/null 2>&1
	r1=$?
	if ((r1 != 0)); then
		LOG_ERR "lost internet"
	fi

	ping -c 1 -W 5 10.8.0.1 >/dev/null 2>&1
	r2=$?
	if ((r2 != 0)); then
		LOG_ERR "lost VPN"
	fi

	if ((r0 == 0 && r1 == 0 && r2 == 0)); then
		time_failed=0
		LOG_INFO "USB2RS485, internet and VPN are OK"
	else
		if ((time_failed >= threshold)); then
			LOG_ERR "try to fix the network"
			reset_network "$1"
			systemctl restart beyond-gui-backend.service
		else
			LOG_ERR "break for $time_failed seconds"
		fi
		((time_failed += interval))

    if (( time_failed > 300 )) ;
	  then
	    systemctl reboot
	  fi

	fi
	sleep $interval
done
