
LOG_DEBUG() {
	logger -p local0.debug -t network-watchdog "$1"
}

LOG_INFO() {
	logger -p local0.info -t network-watchdog "$1"
}

LOG_NOTICE() {
	logger -p local0.notice -t network-watchdog "$1"
}

LOG_WARNING() {
	logger -p local0.warning -t network-watchdog "$1"
}

LOG_ERR() {
	logger -p local0.err -t network-watchdog "$1"
}

#
# stop VPN
#
stop_vpn() {
	LOG_WARNING 'stop VPN'
	systemctl stop openvpn-client@$(hostname)
	sleep 5
}

start_vpn() {
	LOG_WARNING 'start VPN'
	systemctl start openvpn-client@$(hostname)
	sleep 5
}

#
# power off the 4G module
#
power_off_usb() {
	LOG_WARNING 'power off the USB so 4G module can reboot'
	echo 0 >/sys/kernel/debug/musb-hdrc.1/softconnect
	sleep 1
}

#
# power on the 4G module
#
power_on_usb() {
	LOG_WARNING 'power on the USB so 4G module can reboot'
	LOG_WARNING 'wait 60 seconds for 4G module to power on'
	echo 1 >/sys/kernel/debug/musb-hdrc.1/softconnect
	sleep 60
}

reset_wifi() {

	ip link set wlan0 down
	sleep 5
	ip link set wlan0 up
	sleep 5
	iw wlan0 scan
	sleep 5
	#
	# reset the networking
	#
	systemctl restart networking
	sleep 5
	#
	# connect to wifi
	#
	connmanctl connect "$1"
	sleep 10
}

reset_network() {

	stop_vpn
	# 
	# power_off_usb 
	# power_on_usb

	while true; do
		if ping -c 1 -W 5 google.com >/dev/null; then
			LOG_NOTICE 'internet and DNS is OK'
			break
		else
			LOG_WARNING 'try to reset wifi now'
			reset_wifi "$1"
		fi
	done

	while true; do
		if ping -c 1 -W 5 10.8.0.1 >/dev/null; then
			LOG_NOTICE 'VPN is OK'
			break
		else
			LOG_WARNING 'try to start VPN now'
			start_vpn
		fi
	done
}
