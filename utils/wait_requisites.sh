#!/bin/bash
LOG_INFO() {
	logger -p local0.info -t pluto "$1"
}

LOG_NOTICE() {
	logger -p local0.notice -t pluto "$1"
}

LOG_WARNING() {
	logger -p local0.warning -t pluto "$1"
}

LOG_ERR() {
	logger -p local0.err -t pluto "$1"
}

if (( $# == 0 ))
then
	LOG_ERR  "please specify the serial device"
	exit 1
else
	LOG_NOTICE  "predefined Serial is $1"
fi

wait_time=0

while true 
do
	LOG_NOTICE "checking Serial"
	if [ -c "$1" ] ;
	then
		LOG_NOTICE "Serial $1 is OK"
		break
	else
		LOG_WARNING "wait 10 more seconds, $wait_time seconds in total."
		((wait_time+=10))
		sleep 10
	fi

	if (( wait_time > 300 )) ;
	then
	  systemctl reboot
	fi

done
