import logging
import itertools
import time
from colorlog import ColoredFormatter

ERROR_CODE_NO_CONFIGURATION_FILE = 100
ERROR_CODE_MODBUS_COMM_LOST = 101
ERROR_CODE_CONFIG_INVALID = 102
ERROR_CODE_NO_SSL_CERT = 103
ERROR_CODE_NO_SSL_PRIVATE_KEY = 104

config = {}


def setup_logger(name: str, verbose: int = 1):
	"""Return a logger with a default ColoredFormatter."""
	formatter = ColoredFormatter("%(log_color)s%(levelname)-8s %(bold)s%(asctime)s.%(msecs)03d %(blue)s%(name)-8s%(reset)s %(message)s",
									datefmt='%H:%M:%S',
									reset=True,
									log_colors={
										'DEBUG': 'cyan',
										'INFO': 'green',
										'WARNING': 'yellow',
										'ERROR': 'red',
										'CRITICAL': 'red',
									})

	logger = logging.getLogger(name)

	handler = logging.StreamHandler()
	handler.setFormatter(formatter)
	logger.addHandler(handler)

	# fh = logging.FileHandler('logging.log')
	# fh.setLevel(logging.WARNING)
	# fh.setFormatter(logging.Formatter('%(asctime)s; %(name)s - %(message)s'))

	# logger.addHandler(fh)
	if verbose >= 2:
		logger.setLevel(logging.DEBUG)
	elif verbose >= 1:
		logger.setLevel(logging.INFO)
	else:
		logger.setLevel(logging.WARNING)

	return logger


logger = setup_logger('autotest')

spinner = itertools.cycle(['-', '/', '|', '\\'])


def get_serial() -> str:
	# we use the first mac address as the serial number for the cloud module
	import subprocess
	addr = subprocess.check_output(['ip', 'addr']).decode('utf8')
	addr = addr.split('\n')

	macs = []
	for l in addr:
		if 'ether' in l: macs.append(l)

	if len(macs) > 0:
		m_index = macs[0].find('ether') + 6
		mac = macs[0][m_index:m_index + 17]
		mac = ''.join(mac.split(':'))
		return mac
	else:
		logger.error('failed to get mac address')
		return None


if __name__ == "__main__":
	log = setup_logger('testing', verbose=2)
	log.info('info: say something.')
	log.debug(('info: say something. ', 'comes from a tuple ', 54545))
	print(get_serial())
	while True:
		print(next(spinner), end='\r', flush=True)
		time.sleep(0.2)
