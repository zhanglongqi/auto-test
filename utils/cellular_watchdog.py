#!/usr/bin/env python3
import logging
import signal
import subprocess
import os
import time
import gpiod

logging.basicConfig(level=logging.INFO)

TO_EXIT = False


def cellular_restart(target: str, gpiochip: str, gpioline: str):
	logging.info(f'cellular_restart on {target} {gpiochip} {gpioline}')

	# 1. stop the dial-up process
	process_ongoing = True
	while process_ongoing:
		process = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
		output, error = process.communicate()

		for line in output.splitlines():
			if target in str(line):
				pid = int(line.split(None, 1)[0])
				os.kill(pid, 9)
		process = subprocess.Popen(['systemctl', 'stop', 'pcie-4g.service'], stdout=subprocess.PIPE)
		output, error = process.communicate()

		time.sleep(2)
		process = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
		output, error = process.communicate()
		process_ongoing = False
		for line in output.splitlines():
			if target in str(line):
				process_ongoing = True
				break

	# 2. reset the mini-PCIE 4G module
	reset_line = gpiod.chip(gpiochip).get_line(gpioline)

	config = gpiod.line_request()
	config.consumer = "pcie_reset"
	config.request_type = gpiod.line_request.DIRECTION_OUTPUT

	reset_line.request(config)
	reset_line.set_value(0)
	time.sleep(5)
	reset_line.set_value(1)

	# 3. start the dial-up process
	time.sleep(10)
	process = subprocess.Popen(['systemctl', 'restart', 'pcie-4g.service'], stdout=subprocess.PIPE)
	output, error = process.communicate()


def monitor_network(interface: str, target: str, gpiochip, gpioline):
	lost_threshold = 5
	lost_counter = 0
	while True:
		res = subprocess.run(['ping', '-W', '5', '-c', '1', '-I', interface, 'ims.xnergy.tech'], capture_output=True, text=True)
		logging.debug(res.stdout)

		if 0 != res.returncode or res.stderr:
			lost_counter += 1
			logging.error(f'ping failed {interface},{res.returncode},{res.stderr}')
		else:
			lost_counter = 0
			logging.info(f'ping successfully on {interface}')
		if lost_counter >= lost_threshold:
			lost_counter = 0
			logging.error(f'lost network on {interface}, reset the cellular now')
			logging.info('the monitoring will continue after 10mins')
			cellular_restart(target=target, gpiochip=gpiochip, gpioline=gpioline)
			time.sleep(600)
		if TO_EXIT:
			logging.info('stop monitoring cellular newtwork')
			break
		time.sleep(60)


def my_custom_handler(sig_num, stack_frame):
	print(f'I have encountered the signal {sig_num} {stack_frame}')
	global TO_EXIT
	TO_EXIT = True


if __name__ == '__main__':

	# import shlex
	# shlex.split('/bin/prog -i data.txt -o "more data.txt"')

	# signal.signal(signal.SIGINT, my_custom_handler)
	signal.signal(signal.SIGTERM, my_custom_handler)
	# signal.alarm(10)
	import argparse

	parser = argparse.ArgumentParser(description='kill cellular dial-up process to retry the dail-up')

	parser.add_argument('--target', type=str, nargs="?", default='pppd', help='the cellular dial-up process name')
	parser.add_argument('--gpiochip', type=str, nargs="?", default='gpiochip0', help='the gpio chip of mini-PCIE reset')
	parser.add_argument('--gpioline', type=int, nargs="?", default=6, help='the gpio line of mini-PCIE reset')

	parser.add_argument('--interface', type=str, nargs="?", default='ppp0', help='the network interface to monitor')

	args = parser.parse_args()

	# cellular_restart(args.process)
	monitor_network(args.interface, args.target, args.gpiochip, args.gpioline)
