#!/usr/bin/env python3
import sys
import time
import signal

from charger.daemon import ChargerThread

VERSION = '0.1.0'

do_quit = False


def signal_handler(sig, frame):
	global do_quit
	do_quit = True


signal.signal(signal.SIGTERM, signal_handler)


def run_app():
	import argparse

	parser = argparse.ArgumentParser(
		description='auto test the charger',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)

	parser.add_argument('device', type=str, help='The serial device connected to xnergy charger')
	parser.add_argument('-p', '--parallel', action='store_true', help='charger type, parallel or not')

	parser.add_argument('-V', '--version', action='version', version=VERSION)
	parser.add_argument('--start-charging', action='store_true', help='start the charging')
	parser.add_argument('--stop-charging', action='store_true', help='stop the charging')

	args = parser.parse_args()

	charger_thread = ChargerThread(device=args.device, is_parallel=args.parallel)
	if args.start_charging:
		charger_thread.enable_charging()
	elif args.stop_charging:
		charger_thread.disable_charging()
	else:
		charger_thread.start()

	while True:
		# time.sleep(2)
		# continue
		if not charger_thread.is_alive():
			sys.exit(4)
		else:
			time.sleep(2)
		if do_quit:
			charger_thread.stop_quit()
			charger_thread.join()
			break


if __name__ == "__main__":
	run_app()
