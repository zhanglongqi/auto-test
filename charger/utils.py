def mask2value(mask: int, origin: int):
	'''
	get new value from origin using mask then right shift to proper position
	eg:
		mask 0x0000FF00 origin 0x12345678
		return 0x56
	'''
	return (origin & mask) >> (len(f'{mask:b}') - f'{mask:b}'.rfind('1') - 1)


MASK_SERIES = 0xFFF00000
MASK_UNIT_TYPE = 0x000F0000
MASK_UNIT_SPEC = 0x0000FF00
MASK_REV = 0x000000F0
MASK_SUB_REV = 0x0000000F


class Hardware_Revision:
	def __init__(self, rev: int):
		self.series = self.series2str(mask2value(MASK_SERIES, rev))
		self.type = self.unit_type2str(mask2value(MASK_UNIT_TYPE, rev))
		self.spec = self.unit_spec2str(mask2value(MASK_UNIT_SPEC, rev))
		self.revision = self.revision2str(mask2value(MASK_REV, rev))
		self.sub_revision = self.sub_revision2str(mask2value(MASK_SUB_REV, rev))

	def series2str(self, series):
		if 1 == series:
			return 'beyond'
		else:
			return 'unknown'

	def unit_type2str(self, unit_type):
		if 1 == unit_type:
			return 'tpu'
		elif 2 == unit_type:
			return 'rcu'
		else:
			return 'unknown'

	def unit_spec2str(self, unit_spec):
		if 'tpu' == self.type:
			if 0 == unit_spec:
				return '3kW'
			elif 1 == unit_spec:
				return '1kW'
			else:
				return 'unknown'
		elif 'rcu' == self.type:
			if 0 == unit_spec:
				return '24V_90A'
			elif 1 == unit_spec:
				return '48V_50A'
			elif 2 == unit_spec:
				return '24V_45A'
			elif 3 == unit_spec:
				return '48V_30A'
			else:
				return 'unkown'
		else:
			return 'unknown'

	def revision2str(self, revision):
		if 9 == revision:
			return 'pre_rev_A'
		elif 0xA == revision:
			return 'rev_A'
		elif 0xB == revision:
			return 'rev_B'
		elif 0xC == revision:
			return 'rev_C'
		elif 0xD == revision:
			return 'rev_D'
		elif 0xE == revision:
			return 'rev_E'
		elif 0xF == revision:
			return 'rev_F'

	def sub_revision2str(self, sub_revision):
		if 0 == sub_revision:
			return 'original'
		else:
			return f'sub_rev_{sub_revision}'

	def __str__(self) -> str:
		return f'{self.series}\t{self.type}\t{self.spec}\t{self.revision}.{self.sub_revision}'


if __name__ == '__main__':
	print(hex(mask2value(mask=0x0000FF00, origin=0x12345678)))

	rcu_rev = Hardware_Revision(0x001201A1)
	print(rcu_rev)

	tpu_rev = Hardware_Revision(0x001101A1)
	print(tpu_rev)
