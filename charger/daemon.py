from threading import Thread
import minimalmodbus
import serial
import time
import logging
import sys
import random

from utils import logger

_MODBUS_READ_COIL = int("0x01", 16)
_MODBUS_READ_DISCRETE = int("0x02", 16)
_MODBUS_READ_HOLDING_REGISTERS = int("0x03", 16)
_MODBUS_READ_INPUT_REGISTERS = int("0x04", 16)
_MODBUS_WRITE_SINGLE_REGISTER = int("0x06", 16)

_BAUDRATE = 9600
CHARGER_UNIT_ID = 16


class ChargerThread(Thread):

	def __init__(self, device: str, is_parallel: bool = False):
		super().__init__()
		self.name = 'charger'
		self.daemon = True
		self.do_quit = False
		self.is_parallel: bool = is_parallel
		logger.info('charger thread is launched')
		try:
			self.modebus = minimalmodbus.Instrument(port=device,
													mode=minimalmodbus.MODE_RTU,
													slaveaddress=CHARGER_UNIT_ID)
		except (FileNotFoundError, serial.serialutil.SerialException):
			logger.error(f'open {device} failed')
			sys.exit(2)
		else:
			self.modebus.serial.baudrate = _BAUDRATE
			self.modebus.serial.timeout = 1

	def enable_charging(self):
		logger.info('enable charging')
		try:
			if self.is_parallel:
				self.modebus.write_register(registeraddress=16, value=1, functioncode=_MODBUS_WRITE_SINGLE_REGISTER)
			else:
				self.modebus.write_bit(registeraddress=0, value=1)
		except Exception:
			logger.error('write register failed, check the connection and charger.')

	def disable_charging(self):
		logger.info('disable charging')
		try:
			if self.is_parallel:
				self.modebus.write_register(registeraddress=16, value=0, functioncode=_MODBUS_WRITE_SINGLE_REGISTER)
			else:
				self.modebus.write_bit(registeraddress=0, value=0)
		except Exception:
			logger.error('write register failed, check the connection and charger.')

	@property
	def charger_voltage(self):
		try:
			if self.is_parallel:
				value = self.modebus.read_registers(registeraddress=42,
													number_of_registers=1,
													functioncode=_MODBUS_READ_INPUT_REGISTERS)
			else:
				value = self.modebus.read_registers(registeraddress=30,
													number_of_registers=1,
													functioncode=_MODBUS_READ_INPUT_REGISTERS)
		except Exception:
			logger.error('get charger voltage failed, check the connection and charger.')
			value = [0]
		return value[0] / 128

	@property
	def battery_voltage(self):
		try:
			if self.is_parallel:
				value = self.modebus.read_registers(registeraddress=43,
													number_of_registers=1,
													functioncode=_MODBUS_READ_INPUT_REGISTERS)
			else:
				value = self.modebus.read_registers(registeraddress=32,
													number_of_registers=1,
													functioncode=_MODBUS_READ_INPUT_REGISTERS)
		except Exception:
			logger.error('get battery voltage failed, check the connection and charger.')
			value = [0]
		return value[0] / 128

	@property
	def current(self):
		try:
			if self.is_parallel:
				value1 = self.modebus.read_registers(registeraddress=41,
														number_of_registers=1,
														functioncode=_MODBUS_READ_INPUT_REGISTERS)
				value2 = self.modebus.read_registers(registeraddress=57,
														number_of_registers=1,
														functioncode=_MODBUS_READ_INPUT_REGISTERS)
				value3 = self.modebus.read_registers(registeraddress=73,
														number_of_registers=1,
														functioncode=_MODBUS_READ_INPUT_REGISTERS)
				value = (value1[0] + value2[0] + value3[0]) / 128
			else:
				value = self.modebus.read_registers(registeraddress=31,
													number_of_registers=1,
													functioncode=_MODBUS_READ_INPUT_REGISTERS)
				value = value[0] / 128
		except Exception:
			logger.error('get current failed, check the connection and charger.')
			value = 0
		return value

	def stop_quit(self):
		# stop the charging and quit the process
		self.disable_charging()
		logger.info('stop charging and quit')
		self.do_quit = True

	def run(self):
		counter = 0
		timer_enable = time.time()
		timer_disable = time.time()
		timeout_enable = 0
		timeout_disable = 0
		charging = False
		while True:
			if self.do_quit:
				break

			if not charging and time.time() - timer_disable > timeout_disable:
				counter += 1
				logger.info(f'round {counter}')
				self.enable_charging()
				timer_enable = time.time()
				timeout_enable = random.randint(180, 240)
				timeout_disable = random.randint(100, 120)
				charging = True

			if charging and time.time() - timer_enable > timeout_enable:
				self.disable_charging()
				timer_disable = time.time()
				charging = False

			if charging:
				logger.info(
					f'round {counter} Charger voltage: {self.charger_voltage:.2f} Battery voltage: {self.battery_voltage:.2f} Current: {self.current:.2f}'
				)

			time.sleep(4)


if __name__ == '__main__':
	logger.setLevel(logging.DEBUG)
	charger_thread = ChargerThread(device='dev/ttyUSB0', is_parallel=False)
	charger_thread.start()
	charger_thread.join()
