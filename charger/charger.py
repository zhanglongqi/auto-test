import time


def status_customer():
	return {
		"type":
		"data",
		"channel":
		0,
		"unit":
		16,
		"data": [{
			"label": "HARDWARE_REV_LO",
			"address": 96,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137599.3974884,
			"__value__": [1],
			"func": None,
			"show_as_hex": True,
			"value": 1
		}, {
			"label": "HARDWARE_REV_HI",
			"address": 97,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137599.4455755,
			"__value__": [2],
			"func": None,
			"show_as_hex": True,
			"value": 2
		}, {
			"label": "FIRMWARE_REV_LO",
			"address": 98,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.2135348,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "FIRMWARE_REV_HI",
			"address": 99,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.261506,
			"__value__": [65518],
			"func": None,
			"show_as_hex": True,
			"value": 65518
		}, {
			"label": "shadow_error_lo",
			"address": 0,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.3095593,
			"__value__": [21309],
			"func": None,
			"show_as_hex": True,
			"value": 21309
		}, {
			"label": "shadow_error_hi",
			"address": 1,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.3572714,
			"__value__": [19344],
			"func": None,
			"show_as_hex": True,
			"value": 19344
		}, {
			"label": "runtime_error_lo",
			"address": 52,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.405217,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "runtime_error_hi",
			"address": 53,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.4532592,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "enable_charger",
			"address": 0,
			"data_type": "bool",
			"data_count": 1,
			"base_count": 1,
			"object_type": "coil",
			"timestamp": 1614137612.5012333,
			"__value__": [0],
			"func": None,
			"show_as_hex": False,
			"value": 0
		}, {
			"label": "Battery Voltage",
			"address": 32,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.5495765,
			"__value__": [16806],
			"func": None,
			"show_as_hex": False,
			"value": 16806
		}]
	}


def status_admin():
	return {
		"type":
		"data",
		"channel":
		0,
		"unit":
		16,
		"data": [{
			"label": "ADMIN HARDWARE_REV_LO",
			"address": 96,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137599.3974884,
			"__value__": [1],
			"func": None,
			"show_as_hex": True,
			"value": 1
		}, {
			"label": "ADMIN HARDWARE_REV_HI",
			"address": 97,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137599.4455755,
			"__value__": [2],
			"func": None,
			"show_as_hex": True,
			"value": 2
		}, {
			"label": "FIRMWARE_REV_LO",
			"address": 98,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.2135348,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "FIRMWARE_REV_HI",
			"address": 99,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.261506,
			"__value__": [65518],
			"func": None,
			"show_as_hex": True,
			"value": 65518
		}, {
			"label": "shadow_error_lo",
			"address": 0,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.3095593,
			"__value__": [21309],
			"func": None,
			"show_as_hex": True,
			"value": 21309
		}, {
			"label": "shadow_error_hi",
			"address": 1,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.3572714,
			"__value__": [19344],
			"func": None,
			"show_as_hex": True,
			"value": 19344
		}, {
			"label": "runtime_error_lo",
			"address": 52,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.405217,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "runtime_error_hi",
			"address": 53,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.4532592,
			"__value__": [0],
			"func": None,
			"show_as_hex": True,
			"value": 0
		}, {
			"label": "enable_charger",
			"address": 0,
			"data_type": "bool",
			"data_count": 1,
			"base_count": 1,
			"object_type": "coil",
			"timestamp": 1614137612.5012333,
			"__value__": [0],
			"func": None,
			"show_as_hex": False,
			"value": 0
		}, {
			"label": "Battery Voltage",
			"address": 32,
			"data_type": "uint",
			"data_count": 1,
			"base_count": 1,
			"object_type": "input",
			"timestamp": 1614137612.5495765,
			"__value__": [16806],
			"func": None,
			"show_as_hex": False,
			"value": 16806
		}]
	}


class TPU_STATE:
	PRI_READY = 0
	PFC_start = 1
	idle = 2
	handshake = 3
	paired = 4
	pre_run = 5
	charging = 6
	stopping = 7
	freeze = 10
	debug_mode = 240
	error = 238
	LOST_CONN = 255
	RCU_ONLINE = (paired, pre_run, charging, stopping)

	def name(self, state):
		if state == self.PFC_start:
			return "PFC_start"
		elif state == self.idle:
			return "idle"
		elif state == self.handshake:
			return "handshake"
		elif state == self.paired:
			return "paired"
		elif state == self.pre_run:
			return "pre_run"
		elif state == self.charging:
			return "charging"
		elif state == self.stopping:
			return "stopping"
		elif state == self.freeze:
			return "freeze"
		elif state == self.debug_mode:
			return "debug_mode"
		elif state == self.error:
			return "error"
		else:
			return f"debug_mode-{state}"


charger = {
	'ts': int(time.time()),
	'session_id': '',
	'energy': 0,
	'session_energy': 0,
}
